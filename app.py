#!/usr/bin/python

from flask import Flask, flash, render_template, request, session
import logging
import ConfigParser
import os,sys

print os.getcwd()
# os.environ["PYTHONPATH"] =  os.getcwd()
# sys.path.append(os.getcwd())

print sys.path

import EmployeesManagement
import MenuManagement
import OrderManagement
import RequestsManagement
import DBAccess

app = Flask(__name__)
#help for keeping up session var - the session var hold the id of user on website while he is loged in
app.secret_key = "123456789"

# show all logs from info level
logging.basicConfig(level=logging.INFO)

# create config var
config = ConfigParser.ConfigParser()
config.read('config.ini')

# db configuration
host = config.get('DB', 'host')
port = config.get('DB', 'port')
user = config.get('DB', 'user')
password = config.get('DB', 'password')
database = config.get('DB', 'database')


@app.route('/')
def home():
    if not session.get('logged_in'):
        return render_template("login.html")
    elif session.get('isManager') == 1:
        return render_template("admin.html")
    else:
        return render_template("index.htm")


@app.route('/login', methods=['POST', 'GET'])
def login():
    """
    auth user id on login and save his details on current session
    """
    if request.method == 'POST':
        print 1
        # if login button clicked
        if request.form['login_button'] == 'login':
            print 2
            misparZihuy = request.form['mispar_zihuy']
            # if mispar zihuy is only numbers
            if misparZihuy.isdigit():
                misparZihuy = int(misparZihuy)
                print 3
                query = config.get('QUERY', 'authQuery') + str(misparZihuy)

                result = DBAccess.DBAccess().checkValuesOnDB(host, user, password, database, query)
                # select mispar zihuy from db and equal him to thisof the website,if mispar zihuy exist in db
                if result is not None:
                    print 4
                    # get db query and put them in vars
                    isManager = int(result[0]['is_manager'])
                    misparZihuyFromDB = int(result[0]['mispar_zihuy'])
                    name = str(result[0]['first_name'] + " " + result[0]['last_name'])
                    id = int(result[0]['employeeId'])
                    # if mispar zihuy exists
                    if misparZihuyFromDB == misparZihuy:
                        print 5
                        #fill sesion in details of user
                        session['logged_in'] = True
                        session['id'] = id
                        session['name'] = name
                        session['isManager'] = isManager
                        return home()
                    else:
                        flash('wrong mispar zihuy')
                        return render_template("login.html")
                else:
                    flash('not results found')
                    return render_template("login.html")
            else:
                flash('mispar zihuy must be only numbers')
                return render_template("login.html")

    elif request.method == 'GET':
        print 6
        return render_template("login.html")
    return home()


@app.route("/logout")
def logout():
    session['logged_in'] = False
    return home()


@app.route("/createOrder", methods=['GET', 'POST'])
def createOrder():
    """
    employee choose items to eat page. transfer data from db to html file to show his choices
    """
    if request.method == 'POST':
        # get checked box value
        itemsId = request.form.getlist('items[]')
        employeeId = int(session['id'])
        orderManagement = OrderManagement.OrderManagement()
        orderManagement.sendOrder(itemsId, employeeId)
        return render_template("successPageEmployees.html")
    elif request.method == 'GET':
        orderManagement = OrderManagement.OrderManagement()
        dataFromDB = orderManagement.getItemsToOrder()
        #check if db connection is dead
        if dataFromDB is None:
            flash("there is no connection with db")
            return render_template("createOrder.html")
        return render_template("createOrder.html", result=dataFromDB)


@app.route("/orderHistory")
def orderHistory():
    """
    get order history to page.transfer the history to html page
    """
    employeeId = int(session['id'])
    orderManagement = OrderManagement.OrderManagement()
    dataFromDB = orderManagement.getOrderHistory(employeeId)
    # check if db connection is dead
    if dataFromDB is None:
        flash("there is no connection with db")
        return render_template("orderHistory.html")
    return render_template("orderHistory.html", result=dataFromDB)


@app.route("/itemRequest", methods=['GET', 'POST'])
def itemRequest():
    """
    request and item page
    """
    if request.method == 'POST':
        # get request item value from html field
        requestedItem = request.form['item'].encode('ascii', 'ignore')
        if requestedItem:
            employeeId = int(session['id'])
            requestManagement = RequestsManagement.RequestsManagement()
            requestManagement.requestItem(employeeId, requestedItem)
            return render_template("successPageEmployees.html")
        else:
            flash("the item must be in english")
            return render_template("itemRequest.html")
    elif request.method == 'GET':
        return render_template("itemRequest.html")


@app.route("/showItemRequests")
def showItemRequests():
    """
    show requests items of employess on page
    """
    if session.get('isManager') == 1:
        requestManagement = RequestsManagement.RequestsManagement()
        dataFromDB = requestManagement.viewRequests()
        # check if db connection is dead
        if dataFromDB is None:
            flash("there is no connection with db")
            return render_template("viewRequests.html")
        return render_template("viewRequests.html", result=dataFromDB)
    else:
        return render_template("index.htm")


@app.route("/showAllEmployees", methods=['GET', 'POST'])
def showAllEmployees():
    if session.get('isManager') == 1:
        employeesManagement = EmployeesManagement.EmployeesManagement()
        if request.method == 'POST':
            # if button remove clicked
            if request.form['remove'] == 'remove':
                # get all checked box value
                employeesIdsToDelete = request.form.getlist('deleteId[]')
                employeesManagement.removeEmployee(employeesIdsToDelete)
                return render_template("successPageAdmins.html")

        if request.method == 'GET':
            dataFromDB = employeesManagement.viewEmployees()
            # check if db connection is dead
            if dataFromDB is None:
                flash("there is no connection with db")
                return render_template("employeesView.html")
            return render_template("employeesView.html", result=dataFromDB)
    else:
        return render_template("index.htm")


@app.route("/addNewEmployee", methods=['GET', 'POST'])
def addNewEmployee():
    if session.get('isManager') == 1:
        employeesManagement = EmployeesManagement.EmployeesManagement()
        if request.method == 'POST':
            # if button add clicked
            if request.form['add'] == 'add':
                # get values from html fields
                dataFromDB = employeesManagement.viewEmployees()
                misparZihuy = str(request.form['mispar_zihuy'])
                firstName = request.form['first_name'].encode('ascii', 'ignore')
                lastName = request.form['last_name'].encode('ascii', 'ignore')
                isManager = request.form.get('is_manager')
                # check if isManager field is check or not
                if isManager is None:
                    isManager = 0
                else:
                    isManager = 1
                # check if field are fill by admin
                if misparZihuy and firstName and lastName:
                    # check if mispar zihuy is only numbers and his length is 9 chars
                    if misparZihuy.isdigit() and len(misparZihuy) == 9:
                        #convert mispar zihuy to int for the DB
                        misparZihuy = int(misparZihuy)
                        # check if db connection is dead
                        if dataFromDB is None:
                            flash("there is no connection with db")
                            return render_template("addNewEmployee.html")
                        for row in dataFromDB:
                            print row['mispar_zihuy'] == misparZihuy
                            print row['mispar_zihuy']
                            print misparZihuy
                            if row['mispar_zihuy'] == misparZihuy:
                                flash('mispar zihuy already exists')
                                return render_template("addNewEmployee.html")
                        employeesManagement.addEmployee(misparZihuy, firstName, lastName, isManager)
                        return render_template("successPageAdmins.html")
                    else:
                        flash('mispar zihuy must be contain only 9 digits')
                        return render_template("addNewEmployee.html")
                elif not misparZihuy:
                    flash('you must fill mispar zihuy')
                    return render_template("addNewEmployee.html")
                elif not firstName:
                    flash('you must fill first name.\nnote that all details must be in english')
                    return render_template("addNewEmployee.html")
                elif not lastName:
                    flash('you must fill last name.\nnote that all details must be in english')
                    return render_template("addNewEmployee.html")

        if request.method == 'GET':
            return render_template("addNewEmployee.html")
    else:
        return render_template("index.htm")

@app.route("/showAllItems")
def showAllItems():
    if session.get('isManager') == 1:
        menuManagement = MenuManagement.MenuManagement()
        dataFromDB = menuManagement.viewItems()
        # check if db connection is dead
        if dataFromDB is None:
            flash("there is no connection with db")
            return render_template("viewItems.html")
        return render_template("viewItems.html", result=dataFromDB)
    else:
        return render_template("index.htm")

@app.route("/addNewItem", methods=['GET', 'POST'])
def addNewItem():
    if session.get('isManager') == 1:
        if request.method == 'POST':
            menuManagement = MenuManagement.MenuManagement()
            dataFromDB = menuManagement.viewItems()
            #check if item wriiten in english
            item = request.form['item'].encode('ascii', 'ignore')
            #if item in english
            if item:
                # check if db connection is dead
                if dataFromDB is None:
                    flash("there is no connection with db")
                    return render_template("addItem.html")
                #check if exist same item on db
                for row in dataFromDB:
                    if row['description'] == item:
                        flash("item already exists in db")
                        return render_template("addItem.html")
                menuManagement.addItem(item)
                return render_template("successPageAdmins.html")

            else:
                flash("item name must be in english")
                return render_template("addItem.html")
        if request.method == 'GET':
            return render_template("addItem.html")
    else:
        return render_template("index.htm")


if __name__ == '__main__':
    app.jinja_env.auto_reload = True
    app.config['TEMPLATES_AUTO_RELOAD'] = True
    app.run(debug=True,host='0.0.0.0')