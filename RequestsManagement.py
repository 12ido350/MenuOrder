import datetime
import logging
import ConfigParser

import DBAccess


class RequestsManagement():

    def __init__(self):
        self._config = ConfigParser.ConfigParser()
        self._config.read('config.ini')

        self._host = self._config.get('DB', 'host')
        self._port = self._config.get('DB', 'port')
        self._user = self._config.get('DB', 'user')
        self._password = self._config.get('DB', 'password')
        self._database = self._config.get('DB', 'database')

    def requestItem(self, employeeId, requestedItem):
        """
        :param employeeId:
        :param requestedItem:
        insert to db the item that the employee request
        :return:
        """
        try:
            logging.info("employee requested a new item")
            dateNow = datetime.datetime.today()
            query = self._config.get('QUERY', 'requestItemQuery') % (employeeId, requestedItem, dateNow)
            DBAccess.DBAccess().insertIntoDB(self._host, self._user, self._password, self._database, query)
        except Exception as e:
            print("Error in SQL:\n", e)
            logging.error("there is no connection with the db")

    def viewRequests(self):
        try:
            logging.info("select all item requests in db")
            query = self._config.get('QUERY', 'getRequestsItems')
            dataFromDB = DBAccess.DBAccess().retrieveFromDB(self._host, self._user, self._password, self._database, query)
            return dataFromDB
        except Exception as e:
            logging.error("there is no connection with the db")
            print("Error in SQL:\n", e)
            return None
