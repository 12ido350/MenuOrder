import logging
import ConfigParser

import DBAccess


class EmployeesManagement():

    def __init__(self):
        self._config = ConfigParser.ConfigParser()
        self._config.read('config.ini')

        self._host = self._config.get('DB', 'host')
        self._port = self._config.get('DB', 'port')
        self._user = self._config.get('DB', 'user')
        self._password = self._config.get('DB', 'password')
        self._database = self._config.get('DB', 'database')

    def updateEmployee(self, employee):
        """
        :Employee employee:
        update employee details on db
        :return:
        """
        logging.info("update employee details on db")

    def removeEmployee(self, employeesIdsToDelete):
        """
        :Employee employee:
        remove employee from db
        :return:
        """
        logging.info("remove an a employee from db")
        for employeeIdToDelete in employeesIdsToDelete:
            employeeIdToDelete = int(employeeIdToDelete)
            query = self._config.get('QUERY', 'deleteEmployeeQuery') + str(employeeIdToDelete)
            try:
                DBAccess.DBAccess().removeRowInDB(self._host, self._user, self._password, self._database, query)
            except Exception as e:
                logging.error("there is no connection with the db")
                print("Error in SQL:\n", e)

    def addEmployee(self, misparZihuy,firstName,lastName,isManager):
        """
        :Employee employee:
        add employee to db
        :return:
        """

        query = self._config.get('QUERY', 'addNewEmployee') % (misparZihuy, firstName,lastName,isManager)
        print query
        try:
            DBAccess.DBAccess().insertIntoDB(self._host, self._user, self._password, self._database, query)
        except Exception as e:
            logging.error("there is no connection with the db")
            print("Error in SQL:\n", e)
        logging.info("add employee to db")


    def viewEmployees(self):
        """
        :Employees(list) employees:
        retrieve all employees from db
        :return:
        """
        try:
            logging.info("select all employees from db")
            query = self._config.get('QUERY', 'viewAllEmployeesQuery')
            dataFromDB = DBAccess.DBAccess().retrieveFromDB(self._host, self._user, self._password, self._database, query)
            for row in dataFromDB:
                if row['is_manager'] == 1:
                    row['is_manager'] = "yes"
                if row['is_manager'] == 0:
                    row['is_manager'] = "no"
            return dataFromDB
        except Exception as e:
            logging.error("there is no connection with the db")
            print("Error in SQL:\n", e)
            return None
