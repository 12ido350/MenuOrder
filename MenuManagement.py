import logging
import ConfigParser

import DBAccess


class MenuManagement():

    def __init__(self):
        self._config = ConfigParser.ConfigParser()
        self._config.read('config.ini')

        self._host = self._config.get('DB', 'host')
        self._port = self._config.get('DB', 'port')
        self._user = self._config.get('DB', 'user')
        self._password = self._config.get('DB', 'password')
        self._database = self._config.get('DB', 'database')

    def addItem(self,item):
        """
        :Item item:
        add item to db
        :return:
        """
        logging.info("add item to db")
        query = self._config.get('QUERY', 'addNewItemQuery') % (item)
        try:
            DBAccess.DBAccess().insertIntoDB(self._host, self._user, self._password, self._database, query)
        except Exception as e:
            logging.error("there is no connection with the db")
            print("Error in SQL:\n", e)



    def updateItem(self,item):
        """
        :Item item:
        update item on db
        :return:
        """
        logging.info("update item details on db")


    def viewItems(self):
        """
        retrieve all items from db
        :return:
        """
        try:
            logging.info("select all items from db")
            query = self._config.get('QUERY', 'allItemsQuery')
            dataFromDB = DBAccess.DBAccess().retrieveFromDB(self._host, self._user, self._password, self._database, query)
            return dataFromDB
        except Exception as e:
            logging.error("there is no connection with the db")
            print("Error in SQL:\n", e)
            return None
