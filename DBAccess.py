import MySQLdb
from abc import ABCMeta
import logging


class DBAccess(object):
    __metaclass__ = ABCMeta

    def insertIntoDB(self, host, user, password, database, query):
        try:
            mydb, cursor = self._connectToDB(host, user, password, database)
            cursor.execute(query)
            mydb.commit()
            logging.info("insert data to db")
            self._closeConnection(cursor, mydb)
        except Exception as e:
            print("Error in SQL:\n", e)
            # raise Exception("no connection")
            return None

    def updateRowInDB(self, host, user, password, database, query):
        try:
            mydb, cursor = self._connectToDB(host, user, password, database)
            cursor.execute(query)
            mydb.commit()
            logging.info("insert data to db")
            self._closeConnection(cursor, mydb)
        except Exception as e:
            # raise Exception("no connection")
            return None

    def removeRowInDB(self, host, user, password, database, query):
        try:
            mydb, cursor = self._connectToDB(host, user, password, database)
            cursor.execute(query)
            mydb.commit()
            logging.info("insert data to db")
            self._closeConnection(cursor, mydb)
        except Exception as e:
            print("Error in SQL:\n", e)
            # raise Exception("no connection")
            return None

    def retrieveFromDB(self, host, user, password, database, query):
        try:
            logging.info(query)
            mydb, cursor = self._connectToDB(host, user, password, database)
            cursor.execute(query)
            data = cursor.fetchall()
            logging.info("select data from db")
            self._closeConnection(cursor, mydb)
            return data
        except Exception as e:
            print("Error in SQL:\n", e)
            # raise Exception("no connection")
            return None

    def checkValuesOnDB(self, host, user, password, database, query):
        try:
            mydb, cursor = self._connectToDB(host, user, password, database)
            cursor.execute(query)
            data = cursor.fetchall()
            logging.info("check data on db")
            self._closeConnection(cursor, mydb)
            if len(data) == 0:
                return None
            else:
                return data
        except Exception as e:
            print("Error in SQL:\n", e)
            # raise Exception("no connection")
            return None

    def _connectToDB(self, host, user, password, database):
        try:
            mydb = MySQLdb.Connect(
                host,
                user,
                password,
                database
            )

            logging.info("connect to db")
            cursor = mydb.cursor(MySQLdb.cursors.DictCursor)
            return mydb, cursor
        except Exception as e:
            print("Error in SQL:\n", e)
            logging.error("Error in SQL:\n", e)
            return None

    def _closeConnection(self, cursor, mydb):
        cursor.close()
        mydb.close()
