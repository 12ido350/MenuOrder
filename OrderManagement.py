import logging
import ConfigParser
import datetime

import DBAccess


class OrderManagement():

    def __init__(self):
        self._config = ConfigParser.ConfigParser()
        self._config.read('config.ini')

        self._host = self._config.get('DB', 'host')
        self._port = self._config.get('DB', 'port')
        self._user = self._config.get('DB', 'user')
        self._password = self._config.get('DB', 'password')
        self._database = self._config.get('DB', 'database')

    def getOrderHistory(self,employeeId):
        """
        :param employeeId:
        select from db the order history of an employee
        :return:query result of orders
        """
        try:
            logging.info("select all order history of an employee")
            query = self._config.get('QUERY', 'orderHIstoryQuery') + str(employeeId)
            dataFromDB = DBAccess.DBAccess().retrieveFromDB(self._host, self._user, self._password, self._database, query)
            return dataFromDB
        except Exception as e:
            logging.error("there is no connection with the db")
            print("Error in SQL:\n", e)
            return None

    def sendOrder(self,itemsId,employeeId):
        """
        :param itemsId:
        :param employeeId:
        send the items choice of an employee to db
        :return:
        """
        logging.info("send order of an employee")
        dateNow = datetime.datetime.today()
        for itemId in itemsId:
            itemId = int(itemId)
            query = self._config.get('QUERY', 'sendOrderQuery') % (employeeId, itemId, dateNow)
            try:
                DBAccess.DBAccess().insertIntoDB(self._host, self._user, self._password, self._database, query)
            except Exception as e:
                logging.error("there is no connection with the db")
                print("Error in SQL:\n", e)

    def getItemsToOrder(self):
        """
        :return: all items from db
        """
        try:
            logging.info("get all items for employees choices")
            query = self._config.get('QUERY','allItemsQuery')
            dataFromDB = DBAccess.DBAccess().retrieveFromDB(self._host, self._user, self._password, self._database, query)
            return dataFromDB
        except Exception as e:
            logging.error("there is no connection with the db")
            print("Error in SQL:\n", e)
            return None